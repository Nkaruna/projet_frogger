typedef struct Game Game;
typedef struct Driver Driver;
typedef struct Entity Entity;

struct Entity {
    int x;
    int y;
    int id;
    int dir;
};

struct Game {
    int h;
    int w;
    unsigned char* background;
    Entity* entity;
};

enum {
    Grass,      //A
    Brique,     //B
    Gras,       //C
    Water,      //D
    Street,     //E
    Taxi,       //F
    Mini,       //G
    Police,     //H
    Ambulance,  //I
    Audi,       //J
    Truck,      //K
    Wood,       //L
    Turtle,     //M
    Turtle_tmp, //N
    Turtle_fin, //O
    But,        //P
    FrogB,      //Q
    Skull,      //R
    TFige,      //S
    NCell
};

enum {
    Frog,    
    NSprite
};

enum {
    Nothing, Left, Right, Up, Down
};

void init_game(const Driver*);

