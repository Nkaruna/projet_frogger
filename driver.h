typedef struct Driver Driver;

struct Driver {
    const Game* game;
    int (*init)(const Game* game);
    void (*start)(void(*callback)(void*));
    int (*get_move)(void);
    void (*draw_bg)(void);
    void (*draw_entity)(int ent_id);
    void (*affiche_stl)(int score,int init_time,int lives);
    void (*game_over)(int init_time,int lives);
    void (*update)(void);
};

