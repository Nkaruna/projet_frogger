#include "game.h"
#include "driver.h"
#include <time.h>
#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

static int init(const Game* game);
static void start(void(*callback)(void*));
static int get_move(void);
static void draw_bg(void);
static void draw_entity(int ent_id);
static void affiche_stl(int score,int init_time,int lives);
static void game_over(int init_time,int lives);
static void update(void);

Driver ncurses_driver = {
    .game = NULL,
    .init = init,
    .start = start,
    .get_move = get_move,
    .draw_bg = draw_bg,
    .draw_entity = draw_entity,
    .affiche_stl = affiche_stl,
    .game_over = game_over,
    .update = update
};

#define GAME (ncurses_driver.game)

static char tiles[NCell] = {'|', ':',':', '~','-','A','B','C','D','E','F','=','0','*','~',' ','O','X','0'};
static const char* sprites[NSprite] = {"ooo@<<<@>>>@^^^@vvv@"};

enum { FPS = 15 };

static int init(const Game* game) {
    int continuer = 0;
    GAME = game;
    initscr();
    curs_set(0);
    noecho();
    halfdelay(1);
    
    WINDOW * menuwin = newwin(25,80,50,5);
    box(menuwin,0,0);
    keypad(menuwin,true);
    while(!continuer){
        refresh();
        wrefresh(menuwin);
        mvprintw(6, 27 ,"Bienvenue dans Frogger");
        mvprintw(9,20,"Appuyez sur une touche pour commencer");


        int touche = getch();


        if(touche > 1){
        endwin();
        delwin(menuwin);
        clear();
        continuer = 1;
        }
    }
    return 0;
}

static void start(void(*callback)(void*)) {
    for(;;) {
        callback(&ncurses_driver);
        clock_t debut = clock();
        usleep(100000);
        clock_t ecoule = clock() - debut;
    }
}

static int get_move(void) {
    int car = getch();
    switch(car) {
        case 'p':
          endwin();
          exit(0);
        case 'z':
            return Up;
        case 'q':
            return Left;
        case 's':
            return Down;
        case 'd':
            return Right;
        default:
            break;
    }
    return Nothing;
}

static void draw_bg(void) {
    int y, x;
    for(y = 0; y < GAME->h; ++y) {
        for(x = 0; x < GAME->w; ++x) {
            int typecell = GAME->background[y * GAME->w + x];
            mvprintw(y, x, "%c", tiles[typecell]);
        }
    }
}

static void draw_entity(int ent_id) {
    static int anim = 0;
    mvprintw(GAME->entity[ent_id].y, GAME->entity[ent_id].x, "%c", sprites[ent_id][4 * GAME->entity[ent_id].dir + anim]);
    anim = (anim + 1) % 1;
}

static void affiche_stl(int score,int init_time,int lives){
    char tab[100];
    sprintf(tab,"Score :%d",score);
    mvprintw(4,50,tab);

    sprintf(tab,"Temps restant :%d",init_time);
    mvprintw(5,50,tab);

    sprintf(tab,"Vies :%d",lives);
  	mvprintw(6,50,tab);
}

static void game_over(int init_time,int lives){
    if(init_time==0 | lives==0){
        sleep(2);
        endwin();
        exit(0);
    }

}
static void update(void) {
    refresh();
}
