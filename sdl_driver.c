#include "game.h"
#include "driver.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

static int init(const Game* game);
static void start(void(*callback)(void*));
static int get_move(void);
static void draw_bg(void);
static void draw_entity(int ent_id);
static void affiche_stl(int score,int init_time,int lives);
static void game_over(int init_time,int lives);
static void update(void);

 
Driver sdl_driver = {
    .game = NULL,
    .init = init,
    .start = start,
    .get_move = get_move,
    .draw_bg = draw_bg,
    .draw_entity = draw_entity,
    .affiche_stl = affiche_stl,
    .game_over = game_over,
    .update = update
};

#define GAME (sdl_driver.game)

static SDL_Window *win;
static SDL_Renderer *ren;
static SDL_Texture* tiles[NCell];
static const char* tiles_files[] = {"files/bg/grass.png", "files/bg/brique.png","files/bg/gras.png", "files/bg/water.png","files/bg/street.png","files/bg/taxi.png","files/bg/mini.png","files/bg/police.png","files/bg/ambulance.png","files/bg/audi.png","files/bg/truck.png","files/bg/wood.png","files/bg/turtle.png","files/bg/turtle_tmp.png","files/bg/water.png","files/bg/but.png","files/bg/Frog.png","files/bg/skull.png","files/bg/turtle.png"};

static SDL_Texture* sprites[NSprite];
static const char* sprites_files[] = { 
"files/sprite/frog.png"};

enum { SZ = 40 };
enum { FPS = 15 };


static int load_sprites(void) {
    int i;
    SDL_Surface *png;
    for(i = 0; i < NSprite; ++i) {
        png = IMG_Load(sprites_files[i]);
        if (png == NULL){
            SDL_DestroyRenderer(ren);
            SDL_DestroyWindow(win);
            printf("Error: %s\n", SDL_GetError());
            SDL_Quit();
            return 1;
        }
        sprites[i] = SDL_CreateTextureFromSurface(ren, png);
        SDL_FreeSurface(png);
    }
    return 0;
}
static int load_frog(void) {
    int i;
    SDL_Surface *png;
    png = IMG_Load(sprites_files[1]);
    if (png == NULL){
        SDL_DestroyRenderer(ren);
        SDL_DestroyWindow(win);
        printf("Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }
    sprites[1] = SDL_CreateTextureFromSurface(ren, png);
    SDL_FreeSurface(png);
    return 0;
}

static int load_tiles(void) {
    int i;
    SDL_Surface *png;
    for(i = 0; i < NCell; ++i) {
        png = IMG_Load(tiles_files[i]);
        if (png == NULL){
            SDL_DestroyRenderer(ren);
            SDL_DestroyWindow(win);
            printf("Error: %s\n", SDL_GetError());
            SDL_Quit();
            return 1;
        }
        tiles[i] = SDL_CreateTextureFromSurface(ren, png);
        SDL_FreeSurface(png);
    }
    return 0;
}

static int load_home(void){
    SDL_Surface * img_home;
    img_home = IMG_Load("files/bg/home.png");
    if (img_home == NULL){
        SDL_DestroyRenderer(ren);
        SDL_DestroyWindow(win);
        printf("Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }
    SDL_Texture * home = SDL_CreateTextureFromSurface(ren, img_home);
    SDL_RenderCopy(ren, home, NULL, NULL);
    SDL_RenderPresent(ren);
    SDL_FreeSurface(img_home);
    return 0;
 }

static int jouer_son(void){
    SDL_Init(SDL_INIT_VIDEO);
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024 ) == -1 ){
        printf("%s", Mix_GetError());
    } 
    Mix_Music *son;
    son = Mix_LoadMUS("files/sound/home.ogg");
    Mix_PlayMusic(son, -1);
    
    return 0;
}

static int init(const Game* game) {
    int i,continuer = 0;
    SDL_Event event;

    GAME = game;
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
        printf("SDL_Init Error: %s\n",  SDL_GetError());
        return 1;
    }
    
    win = SDL_CreateWindow("Game", 0, 0, GAME->w * SZ, GAME->h * SZ, SDL_WINDOW_SHOWN);
    if (win == NULL) {
        printf("SDL_CreateWindow Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }
    
    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ren == NULL){
        SDL_DestroyWindow(win);
        printf("SDL_CreateRenderer Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }
    
    while (!continuer){   
        load_home();
        SDL_WaitEvent(&event);
        if(event.type==SDL_KEYDOWN){
	        continuer=1;
	        break;
	    }				
    }
    
    if(jouer_son())
        return 1;
    if(load_tiles())
        return 1;
    if(load_sprites())
        return 1;
    return 0;
}

static void start(void(*callback)(void*)) {
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop_arg(callback, &sdl_driver, FPS, 0);
#else
    for(;;) {
        callback(&sdl_driver);
        clock_t debut = clock();       
        usleep(130000);
        clock_t ecoule = clock() - debut;
    }
#endif
}

static int get_move(void) {
    SDL_PumpEvents();
    const Uint8 * state = SDL_GetKeyboardState(NULL);

    if (state[SDL_SCANCODE_P]) {
        SDL_Quit();
        exit(0);
    }
    if (state[SDL_SCANCODE_W]|state[SDL_SCANCODE_UP]){
        return Up;    
    }
    if (state[SDL_SCANCODE_S]|state[SDL_SCANCODE_DOWN]) {
        return Down;
    }
    if (state[SDL_SCANCODE_A]|state[SDL_SCANCODE_LEFT]){
        return Left;
    }
    if (state[SDL_SCANCODE_D]|state[SDL_SCANCODE_RIGHT]){
        return Right;
    }
    return Nothing;
    
}

static void draw_bg(void) {
    SDL_RenderClear(ren);
    int y, x;
    SDL_Rect dst = {.x = 0, .y = 0, .w = SZ, .h = SZ };
    for(y = 0; y < GAME->h; ++y) {
        for(x = 0; x < GAME->w; ++x) {
            dst.x = x * SZ;
            dst.y = y * SZ;
            int typecell = GAME->background[y * GAME->w + x];
            SDL_RenderCopy(ren, tiles[typecell], NULL, &dst);
        }
    }
}

static void draw_entity(int ent_id) {
    static int sp = 0;
    SDL_Rect src = {.x = 0, .y = 0, .w = SZ, .h = SZ };
    SDL_Rect dst = {.x = SZ * GAME->entity[ent_id].x, .y = SZ * GAME->entity[ent_id].y, .w = SZ, .h = SZ };
    src.x = sp * 40;
    SDL_RenderCopy(ren, sprites[ent_id], &src, &dst);
    sp = (sp + 1) % 5;
}
static void affiche_stl(int score,int init_time,int lives){
    SDL_Surface * texte = NULL;
    SDL_Texture * affiche;
    SDL_Color blanc = {255, 255, 255};
    SDL_Rect Dest;
    char tab[100];
    
    if(TTF_Init() == -1){
     fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
     exit(EXIT_FAILURE);
    }
    TTF_Font *police = TTF_OpenFont("files/ttf/arial.ttf",50);
    if(!police) {
    printf("TTF_OpenFont: %s\n", TTF_GetError());
    }
     
    sprintf(tab,"Score : %d",score);
    texte = TTF_RenderText_Blended(police,tab, blanc);
    Dest.x = SZ;
    Dest.y = 0;
    Dest.w = 100;
    Dest.h = 30;
    affiche = SDL_CreateTextureFromSurface(ren, texte);
    SDL_RenderCopy(ren, affiche, NULL,&Dest);
    
    sprintf(tab,"Time left: %d",init_time);
    texte = TTF_RenderText_Blended(police,tab, blanc);
    Dest.x = SZ;
    Dest.y = SZ;
    Dest.w = 100;
    Dest.h = 30;
    affiche = SDL_CreateTextureFromSurface(ren, texte);
    SDL_RenderCopy(ren, affiche, NULL,&Dest);

    sprintf(tab,"Vies: %d",lives);
    texte = TTF_RenderText_Blended(police,tab, blanc);
    Dest.x = 800;
    Dest.y = SZ;
    Dest.w = 100;
    Dest.h = 30;
    affiche = SDL_CreateTextureFromSurface(ren, texte);
    SDL_RenderCopy(ren, affiche, NULL,&Dest);
    
    TTF_CloseFont(police);
    TTF_Quit();

}

static void game_over(int init_time, int lives){
    if(init_time==0 | lives ==0){
        sleep(2);
        SDL_Quit();
        exit(0);
    } 

}

static void update(void) {
    SDL_RenderPresent(ren);
}

