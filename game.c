#include "game.h"
#include "driver.h"
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define H 15
#define W 32

Game g;

static const char* level1 = {
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    "CCCCCPCCCCPCCCCPCCCCCPCCCCPCCCCC"
    "DDDLLLLDDDLLLLDDDLLLLDDDLLLLDDDD"
    "DDDDDMMMMMDDDDDSSSSDDDDDSSSSDDDD"
    "DDDLLLLLLLLDDDDDDDDDDDLLLLLLLDDD"
    "DLLLLDDDDLLLLDDDDLLLLDDDDLLLLDDD"
    "DDDDDSSSSSDDDDSSSSSDDDDMMMMMDDDD"
    "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
    "EEEEEKEEEEEEEEEEEEEEEEEEEEKEEEEE"
    "EEEEEEEEEEEEEEEEEEEEEJEEEEEEEEEE"
    "EEEEEEEHEEEEEEEEIEEEEEEEEEHEEEEE"
    "EEEEEEEEEEEEEEGEEEEEEEFEEEEEEGEE"
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
};

static unsigned char bg[H * W];
static int init_time=0,score=0;
static int best_y = 15;
static int lives = 3;
static int temps_max;
time_t depart;

static Entity entity[1];
static int dirx[] = {0, -1, 1, 0, 0 };
static int diry[] = {0, 0, 0, -1, 1 };

static void process_move(int move) {
    entity[0].dir = move;
    int nextx = entity[0].x + dirx[move];
    int nexty = entity[0].y + diry[move];
    
    if(nextx < 0 || nextx >= W || nexty < 0 || nexty >= H || bg[nexty * W + nextx] == Grass||bg[nexty * W + nextx]== Gras||bg[nexty * W + nextx]== FrogB){
        return;
    }
    
    entity[0].x = nextx;
    entity[0].y = nexty;
    
    if (entity[0].x == 31 | entity[0].x == 0){
        entity[0].x = 16;
        entity[0].y = 13;
        lives--;
    }

	


    else if(bg[nexty * W + nextx] == Water || bg[nexty*W+nextx] == Taxi || bg[nexty * W + nextx] == Mini || bg[nexty * W + nextx] == Police ||bg[nexty * W + nextx] == Ambulance ||bg[nexty * W + nextx] == Audi ||bg[nexty * W + nextx] == Truck ||bg[nexty * W + nextx] == Turtle_fin ||bg[nexty * W + nextx] == Skull){
        entity[0].x = 16;
        entity[0].y = 13;
        lives--;
    }
    if(bg[entity[0].y * W + nextx] == Wood){
        ++entity[0].x;
		
    }
    
    if( bg[entity[0].y *W + nextx] == Turtle ||bg[entity[0].y *W + nextx] == TFige || bg[nexty * W + nextx] == Turtle_tmp){
        --entity[0].x;
    }  
}

static void move_auto(){
    int tmp;
    for(int y=3;y<H-2;y++){
        for(int l=0; l<W-1; ++l){
            if(y==4|y==7|y==10|y==12){
                tmp = bg[y*W+l];
                bg[y*W+l] = bg[y*W+l+1];
                bg[y*W+l+1] = tmp;
            }
        }
        for(int r=0; r<W; ++r){
            if(y==3|y==5|y==6|y==9|y==11){
                tmp = bg[y*W+r];
                bg[y*W+r] = bg[y*W];
                bg[y*W] = tmp;
            }
        }
    }
}
/*
static void collision(int move) {
    entity[0].dir = move;
    int nextx = entity[0].x + dirx[move];
    int nexty = entity[0].y + diry[move];

    if (entity[0].x == 32 | entity[0].x == 0){
        entity[0].x = 16;
        entity[0].y = 13;
        lives--;
    }
    //else if(bg[nexty * W + nextx] == Water || bg[nexty*W+nextx] == Taxi || bg[nexty * W + nextx] == Mini || bg[nexty * W + nextx] == Police ||bg[nexty * W + nextx] == Ambulance ||bg[nexty * W + nextx] == Audi ||bg[nexty * W + nextx] == Truck ||bg[nexty * W + nextx] == Turtle_fin ||bg[nexty * W + nextx] == Skull){
    //    entity[0].x = 16;
    //    entity[0].y = 13;
     //   lives--;
    
    else if(bg[nexty * W + entity[0].x] == Wood){
        ++entity[0].x;
    }
    
    else if( bg[nexty *W + nextx] == Turtle || bg[nexty * W + nextx] == Turtle_tmp){
        --entity[0].x;
    }  
}*/

static void rinit(void){
  for(int i  = 0; i < H * W; ++i){
      g.background[i] = level1[i] - 'A';
  }

  entity[0].x = 16;
  entity[0].y = 13;
}

static void but (int move){
    int tmp;
    entity[0].dir = move;
    int nextx = entity[0].x + dirx[move];
    int nexty = entity[0].y + diry[move];
    //entity[0].dir = move;

    if (g.background[49] == FrogB  & g.background[53] == FrogB  & g.background[57] == FrogB & g.background [61] == FrogB & g.background [65] ==  FrogB ){
        score+=1000;
        temps_max +=50;
        sleep(1);
        rinit();
    }
}

static void anim_turtle (void){    
    time(&depart);  
    for(int i = 0; i < H*W; i++){
        if(g.background[i] == Turtle|g.background[i] == Turtle_tmp | g.background[i] == Turtle_fin){
            
            if( g.background[i] == Turtle & (depart%6==0)){
                g.background[i] = Turtle_tmp;

            }
            else if(g.background[i] == Turtle_tmp & (depart%6==2)){
                g.background[i] = Turtle_fin ;

            }
            else if(g.background[i] == Turtle_fin & (depart%6==3)){
                g.background[i] = Turtle_tmp ;

            }
            else if(g.background[i] == Turtle_tmp & (depart%6==4)){
                g.background[i] = Turtle;

            }
        }
    }   
}
static void pop_croco(void){
    int random = rand()%6;
    int tab[5]={69,74,79,85,90};
    static int nenu =5; 
    time(&depart);
    int t; 
    
    for(int i = 0; i<5;i++){
        t=tab[i];
        if(depart%4==2){ 
            if(random==i & nenu==5){
                if(g.background[t] == But & ((depart%3==1)|(depart%3==2))){
                    g.background[t] = Skull;
                    --nenu;
                }
            }
        }
        if(g.background[t] == Skull & (depart%4==1)){ 
            g.background[t] = But;
            ++nenu;                
        }
    }
}
static void calcul_score_time(int move){
    int tmp;
    entity[0].dir = move;
    entity[0].dir = move;
    int nextx = entity[0].x + dirx[move];
    int nexty = entity[0].y + diry[move];
    int temps_min = time(&depart);
    
    if (!temps_max){
    temps_max = temps_min + 50;
    }
    init_time = temps_max - temps_min;

    if(bg[nexty * W + nextx]== But){
        bg[nexty * W + nextx] = FrogB;
        score += 50; //
        score += (5 * init_time);
        best_y = 15;
        g.entity[0].x = 16;
        g.entity[0].y = 13;
    }
    if(entity[0].y  > nexty && entity[0].y < best_y){
      score+=10;
      best_y = entity[0].y;
    }
	if(score >= 20000){
		lives++;
	}
		
}

void save_highscore(){
		FILE *f = fopen("Highscore.txt", "a+");
	if (f == NULL)
	{
    printf("Error opening file!\n");
    exit(1);
	}
	fprintf(f,"\nLe highscore est de : %d ",score);
	fclose(f);
}


static void callback(void* d) {
    const Driver* dr = (Driver*)d;
    int move = dr->get_move();
    dr->draw_bg();
    for(int i=0; i<NSprite;++i){
        dr->draw_entity(i);
    }
    process_move(move);
    //collision(move);
    move_auto();
    but(move);
    anim_turtle();
    pop_croco();
    calcul_score_time(move);
    dr->affiche_stl(score,init_time,lives);
    dr->game_over(init_time,lives);
    dr->update();
}

void init_game(const Driver* dr) {
    int i,x,y;
    g.h = H;
    g.w = W;
    g.background = bg;
    g.entity = entity;

    for(i = 0; i < H * W; ++i){
        g.background[i] = level1[i] - 'A';
    }
    
    entity[0].x = 16;
    entity[0].y = 13;
    entity[0].id = 0;
    
    
    dr->init(&g);
    dr->start(callback);
}

// 4 3 1 2
